﻿using CacheDB;
using Common;
using System;
using System.ServiceModel;
using System.Windows.Forms;

namespace Client
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            var serviceAddress = "127.0.0.1:10000";
            var serviceName = "MyService";
            Uri tcpUri = new Uri($"net.tcp://{serviceAddress}/{serviceName}");
            EndpointAddress address = new EndpointAddress(tcpUri);
            NetTcpBinding clientBinding = new NetTcpBinding();
            ChannelFactory<IDAOEntity> factory = new ChannelFactory<IDAOEntity>(clientBinding, address);
            var DAO = factory.CreateChannel();

            ContextDB contextDB = new ContextDB(DAO);
            contextDB.Add(typeof(User));
            contextDB.Add(typeof(Role));
            contextDB.Add(typeof(UserRoleRelation));
            contextDB.LoadDataFromDB();
            contextDB.GetAll(out ListSyncDB<User> collection01);
            contextDB.GetAll(out ListSyncDB<Role> collection02);
            contextDB.GetAll(out ListSyncDB<UserRoleRelation> collection03);
            collection01[0].Firstname = $"{collection01[0].Firstname}555";
            collection01.RemoveAt(2);
            User user = new User() { Firstname = "Иван", Surname = "Иванов", Secondname = "Иванович" };
            collection01.Add(user);
            contextDB.SyncCollectionWithTableDB();

            InitializeComponent();
        }
    }
}
