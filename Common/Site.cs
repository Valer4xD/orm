﻿using CacheDB;

namespace Common
{
    [DBTableName("my_site")]
    public class Site : DomainObject
    {
        [DBTableColumnName("url")]
        public string URL { get; set; }
    }
}
