﻿using CacheDB;

namespace Common
{
    [DBTableName("user_role")]
    public class UserRoleRelation : DomainObject
    {
        private long _userID;
        [DBTableColumnName("user_id")]
        public long UserID { get { return _userID; } set { TrySaveOriginalThis(); _userID = value; } }

        private long _roleID;
        [DBTableColumnName("role_id")]
        public long RoleID { get { return _roleID; } set { TrySaveOriginalThis(); _roleID = value; } }
    }
}
