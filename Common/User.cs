﻿using CacheDB;
using System.Runtime.Serialization;

namespace Common
{
    [DBTableName("my_user")]
    public class User : DomainObject
    {
        private string _firstname;
        [DBTableColumnName("firstname")]
        public string Firstname { get { return _firstname; } set { TrySaveOriginalThis(); _firstname = value; } }

        private string _surname;
        [DBTableColumnName("surname")]
        public string Surname { get { return _surname; } set { TrySaveOriginalThis(); _surname = value; } }

        private string _secondname;
        [DBTableColumnName("secondname")]
        public string Secondname { get { return _secondname; } set { TrySaveOriginalThis(); _secondname = value; } }

        private ListSyncDB<Role> _roles;
        public ListSyncDB<Role> Roles
        {
            get
            {
                SetCollection<Role, UserRoleRelation>(ref _roles/*, "UserID", "RoleID"*/);
                return _roles;
            }
        }
    }
}
