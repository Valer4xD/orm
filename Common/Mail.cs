﻿using CacheDB;

namespace Common
{
    [DBTableName("my_mail")]
    public class Mail : DomainObject
    {
        [DBTableColumnName("address")]
        public string Name { get; set; }
    }
}
