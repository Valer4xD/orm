﻿using CacheDB;

namespace Common
{
    [DBTableName("my_role")]
    public class Role : DomainObject
    {
        private string _name;
        [DBTableColumnName("name")]
        public string Name { get { return _name; } set { TrySaveOriginalThis(); _name = value; } }
    }
}
