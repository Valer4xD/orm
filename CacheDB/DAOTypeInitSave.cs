﻿using System.Collections.Generic;
using System.Reflection;
using static CacheDB.DAOEntity;

namespace CacheDB
{
    internal class DAOTypeInitSave
    {
        internal readonly List<string> _columnNameList,
                                       _columnTypeList;

        internal readonly TypeCache _typeCache;
        internal readonly string _textSelectCommandSQLFullLoad,
                                 _textSelectCommandSQLTableCache,
                                 _textSelectCommandSQLRowCache;

        internal readonly string _textDeleteCommandSQLPrefix,
                                 _textUpdateCommandSQLPrefix,
                                 _textInsertCommandSQLPrefix;

        internal DAOTypeInitSave(
            List<string> columnNameList,
            List<string> columnTypeList,
                TypeCache typeCache,
                string textSelectCommandSQLFullLoad,
                string textSelectCommandSQLTableCache,
                string textSelectCommandSQLRowCache,
                    string textDeleteCommandSQLPrefix,
                    string textUpdateCommandSQLPrefix,
                    string textInsertCommandSQLPrefix)
        {
            _columnNameList = columnNameList;
            _columnTypeList = columnTypeList;
                _typeCache = typeCache;
                _textSelectCommandSQLFullLoad = textSelectCommandSQLFullLoad;
                _textSelectCommandSQLTableCache = textSelectCommandSQLTableCache;
                _textSelectCommandSQLRowCache = textSelectCommandSQLRowCache;
                    _textDeleteCommandSQLPrefix = textDeleteCommandSQLPrefix;
                    _textUpdateCommandSQLPrefix = textUpdateCommandSQLPrefix;
                    _textInsertCommandSQLPrefix = textInsertCommandSQLPrefix;
        }
    }
}
