﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace CacheDB
{
    internal class CollectionFromTableDB<T> where T : DomainObject, new()
    {
        private IDAOEntity _IDAOEntity;
        private ListSyncDB<T> _listEntity = new ListSyncDB<T>();
        private string _updateTimestamp;
        private bool _firstLoad = true;
        private long _currentUserID;

        private static bool _init = false;
        private static List<PropertyInfo> _properties = new List<PropertyInfo>();
        private static List<string> _columnNameList = new List<string>();

        internal enum StateRowTableDB : byte
        {
            Delete,
            Update,
            Insert
        }

        private static ConcurrentDictionary<long, CollectionFromTableDB<T>> _instanceCollectionUserDictionary
            = new ConcurrentDictionary<long, CollectionFromTableDB<T>>();
        private static object _syncRoot = new object();
        private static ConcurrentDictionary<long, DateTime> _timeLastCallUserDictionary
            = new ConcurrentDictionary<long, DateTime>();
        private const int _downtime = 28800;
        private const int _intervalBetweenChecksDowntime = 60;
        private DateTime _timeLastCheck = DateTime.UtcNow;
        private CollectionFromTableDB() {}
        internal static CollectionFromTableDB<T> GetInstance(long CurrentUserID = 0)
        {
            if( ! _instanceCollectionUserDictionary.TryGetValue(CurrentUserID, out CollectionFromTableDB<T> instance))
                lock (_syncRoot)
                    if( ! _instanceCollectionUserDictionary.ContainsKey(CurrentUserID))
                    {
                        instance = new CollectionFromTableDB<T>();
                        instance._currentUserID = CurrentUserID;
                        _instanceCollectionUserDictionary.TryAdd(CurrentUserID, instance);
                        _timeLastCallUserDictionary.TryAdd(CurrentUserID, DateTime.UtcNow);
                    }
            return instance;
        }
        private void CheckDowntime()
        {
            if((DateTime.UtcNow - _timeLastCheck).TotalSeconds <= _intervalBetweenChecksDowntime)
                return;
            _timeLastCheck = DateTime.UtcNow;

            lock (_syncRoot)
            {
                long UserID;
                int lastID = _timeLastCallUserDictionary.Count - 1;
                for (int u = lastID; u >= 0; --u)
                    if((DateTime.UtcNow - _timeLastCallUserDictionary[u]).TotalSeconds > _downtime)
                    {
                        UserID = _timeLastCallUserDictionary.ElementAt(u).Key;
                        if(_currentUserID != UserID)
                        {
                            _timeLastCallUserDictionary.TryRemove(UserID, out DateTime dateTimeUtc);
                            _instanceCollectionUserDictionary.TryRemove(UserID, out CollectionFromTableDB<T> instance);
                        }
                    }
            }
        }

        internal ListSyncDB<T> GetAll()
        {
            CheckDowntime();
            if( ! _init)
                NoticeError.SyncWithDBThrow();
            return _listEntity;
        }

        internal bool Init(IDAOEntity @IDAOEntity)
        {
            if( ! _init)
                lock (_syncRoot)
                {
                    if(_init)
                        return true;

                    _IDAOEntity = @IDAOEntity ?? _IDAOEntity;
                    if (_IDAOEntity == null)
                        NoticeError.Throw(new ArgumentException());

                    var tableName = string.Empty;
                    if( ! InitTableName()
                    ||  ! InitColumnsSettings())
                        return false;

                    _IDAOEntity.Init(typeof(T).FullName, tableName, _columnNameList);

                    _init = true;

                    bool InitTableName()
                    {
                        var typeAttribute = typeof(DBTableNameAttribute);
                        var customAttributes = typeof(T).GetCustomAttributes(typeAttribute, false);
                        var typeFullName = typeof(T).FullName;
                        var nameAttribute = typeAttribute.Name;
                        switch (customAttributes.Length)
                        {
                            case 0:
                                NoticeError.Throw($@"Для класса ""{typeFullName}"" не указан атрибут ""{nameAttribute}"".");
                                return false;
                            case 1:
                                break;
                            default:
                                NoticeError.Throw($@"Для класса ""{typeFullName}"" указано более 1-го атрибута ""{nameAttribute}"".");
                                return false;
                        }
                        tableName = ((DBTableNameAttribute)customAttributes[0]).TableName;
                        return true;
                    }

                    bool InitColumnsSettings()
                    {
                        var allProperties = typeof(T).GetProperties();
                        int allPropertiesCount = allProperties.Length;
                        PropertyInfo propertyInfo;
                        object[] customAttributes;
                        var typeAttribute = typeof(DBTableColumnNameAttribute);
                        var typeFullName = typeof(T).FullName;
                        var nameAttribute = typeAttribute.Name;
                        for (int p = 0; p < allPropertiesCount; ++p)
                        {
                            propertyInfo = allProperties[p];
                            customAttributes = propertyInfo.GetCustomAttributes(typeAttribute, false);
                            switch (customAttributes.Length)
                            {
                                case 0:
                                    break;
                                case 1:
                                    _properties.Add(propertyInfo);
                                    _columnNameList.Add(((DBTableColumnNameAttribute)customAttributes[0]).ColumnName);
                                    break;
                                default:
                                    NoticeError.Throw($@"Для свойства {propertyInfo.Name} класса ""{typeFullName}"" указано более 1-го атрибута ""{nameAttribute}"".");
                                    return false;
                            }
                        }
                        if (_properties.Count < 2)
                        {
                            NoticeError.Throw($@"В классе ""{typeFullName}"" меньше 2-х ""{nameAttribute}"".");
                            return false;
                        }
                        return true;
                    }
                }
            return true;
        }

        internal bool LoadDataFromDB(DataSet dataSet = null)
        {
            if( ! _init)
            {
                NoticeError.SyncWithDBThrow();
                return false;
            }
            else
            {
                _listEntity._loadDataFromDB = true;

                _listEntity._conflicts.Clear();

                SavetListID();
                _listEntity.Sort(delegate (T first, T second)
                    {return first.ID.CompareTo(second.ID);});

                var result = LoadTable(dataSet);

                _listEntity.Sort(delegate (T first, T second)
                    {return first.ListID.CompareTo(second.ListID);});

                _listEntity._loadDataFromDB = false;

                return result;
            }
        }

        private void SavetListID()
        {
            int count = _listEntity.Count;
            for (int e = 0; e < count; ++e)
                _listEntity[e].ListID = e;
        }

        private bool LoadTable(DataSet dataSet = null)
        {
            try
            {
                int r,
                    countRows;
                DataRow row;
                T entityDB;
                int c,
                    countColumns = _properties.Count;
                bool isConflict;
                SearchInCollection<T> search = new SearchInCollection<T>();
                int e,
                    countEntities = _listEntity.Count,
                    listIDAdded = countEntities;
                int left = 0,
                    right = countEntities - 1,
                    listID = 0;
                long ID;

                var update_timestamp = $"'{DateTime.UtcNow.AddTicks(-10):yyyy-MM-dd HH:mm:ss.ffffff}'";

                if( dataSet == null
                && ! _IDAOEntity.AdapterFill(typeof(T).FullName, out dataSet, _updateTimestamp, GetIDs(_listEntity), _firstLoad))
                    return false;

                if (dataSet.Tables?.Count != 1)
                {
                    NoticeError.SyncWithDBThrow();
                    return false;
                }
                else
                {
                    countRows = dataSet.Tables[0].Rows.Count;
                    if (countRows > 0)
                        switch (_IDAOEntity.GetTypeCache(typeof(T).FullName))
                        {
                            case DAOEntity.TypeCache.NotCache:
                            case DAOEntity.TypeCache.TableCache:
                                for (e = 0; e < countEntities; ++e)
                                    _listEntity[e]._isDelete = true;

                                for (r = 0; r < countRows; ++r)
                                {
                                    row = dataSet.Tables[0].Rows[r];

                                    entityDB = new T();
                                    entityDB._loadDataFromDB = true;
                                    for (c = 0; c < countColumns; ++c)
                                        _properties[c].SetValue(entityDB, row.ItemArray[c]);
                                    entityDB._loadDataFromDB = false;

                                    ID = (long)row["id"];
                                    if (right >= 0
                                    && search.InterpolationListID(_listEntity, ID, left, right, out listID))
                                    {
                                        _listEntity[listID]._isDelete = false;

                                        isConflict = false;
                                        if (_listEntity[listID].State == DomainObject.StateLocal.Update)
                                            for (c = 0; c < countColumns; ++c)
                                                if(row.ItemArray[c].ToString() != _properties[c].GetValue(_listEntity[listID]._original).ToString()
                                                && row.ItemArray[c].ToString() != _properties[c].GetValue(_listEntity[listID]).ToString())
                                                {
                                                    isConflict = true;
                                                    break;
                                                }

                                        if( ! isConflict)
                                        {
                                            if(_listEntity[listID].State != DomainObject.StateLocal.Update)
                                            {
                                                _listEntity._changesDB._updateDB.Add(entityDB);
                                                _listEntity[listID] = entityDB;
                                            }
                                        }
                                        else
                                            _listEntity._conflicts.Add(
                                                original: (T)_listEntity[listID]._original,
                                                local: _listEntity[listID],
                                                other: entityDB);

                                        left = listID;
                                    }
                                    else
                                        if(_listEntity._deleteLocal.Count == 0
                                        || ! search.InterpolationListID(_listEntity._deleteLocal, ID, 0, _listEntity._deleteLocal.Count - 1, out listID))
                                        {
                                            _listEntity._changesDB._insertDB.Add(entityDB);

                                            if (right >= 0
                                            && ID < _listEntity[right].ID)
                                            {
                                                search.TryGetInsertListID(_listEntity, ID, left, right, out left);
                                                _listEntity.InsertInternal(left, entityDB);
                                            }
                                            else
                                                _listEntity.AddInternal(entityDB);
                                        }
                                }

                                for (e = 0; e < countEntities; ++e)
                                    if(_listEntity[e]._isDelete
                                    && _listEntity[e].State != DomainObject.StateLocal.Insert)
                                    {
                                        _listEntity._changesDB._deleteIdentifiersDB.Add(_listEntity[e].ID);
                                        _listEntity.RemoveAtInternal(e);
                                    }
                                break;
                            case DAOEntity.TypeCache.RowCache:
                                if(_firstLoad)
                                    goto case DAOEntity.TypeCache.TableCache;

                                for (r = 0; r < countRows; ++r)
                                {
                                    row = dataSet.Tables[0].Rows[r];
                                    switch((StateRowTableDB)Convert.ToByte(row["state"]))
                                    {
                                        case StateRowTableDB.Delete:
                                            if (right >= 0
                                            && ! search.InterpolationListID(_listEntity, (long)row["id"], left, right, out listID))
                                            {
                                                NoticeError.SyncWithDBThrow();
                                                return false;
                                            }

                                            _listEntity._changesDB._deleteIdentifiersDB.Add(_listEntity[listID].ID);
                                            _listEntity.RemoveAtInternal(listID);
                                            left = listID > 0 ? listID - 1 : 0;
                                            right--;
                                            break;

                                        case StateRowTableDB.Update:
                                            if (right >= 0
                                            && ! search.InterpolationListID(_listEntity, (long)row["id"], left, right, out listID))
                                            {
                                                NoticeError.SyncWithDBThrow();
                                                return false;
                                            }

                                            entityDB = new T();
                                            entityDB._loadDataFromDB = true;
                                            for (c = 0; c < countColumns; ++c)
                                                _properties[c].SetValue(entityDB, row.ItemArray[c]);
                                            entityDB._loadDataFromDB = false;

                                            isConflict = false;
                                            if (_listEntity[listID].State == DomainObject.StateLocal.Update)
                                                for (c = 0; c < countColumns; ++c)
                                                    if(row.ItemArray[c].ToString() != _properties[c].GetValue(_listEntity[listID]._original).ToString()
                                                    && row.ItemArray[c].ToString() != _properties[c].GetValue(_listEntity[listID]).ToString())
                                                    {
                                                        isConflict = true;
                                                        break;
                                                    }

                                            if( ! isConflict)
                                            {
                                                if(_listEntity[listID].State != DomainObject.StateLocal.Update)
                                                {
                                                    _listEntity._changesDB._updateDB.Add(entityDB);
                                                    _listEntity[listID] = entityDB;
                                                }
                                            }
                                            else
                                                _listEntity._conflicts.Add(
                                                    original: (T)_listEntity[listID]._original,
                                                    local: _listEntity[listID],
                                                    other: entityDB);

                                            left = listID;
                                            break;

                                        case StateRowTableDB.Insert:
                                            entityDB = new T();
                                            entityDB._loadDataFromDB = true;
                                            for (c = 0; c < countColumns; ++c)
                                                _properties[c].SetValue(entityDB, row.ItemArray[c]);
                                            entityDB._loadDataFromDB = false;
                                            entityDB.ListID = listIDAdded++;

                                            ID = (long)row["id"];
                                            if (_listEntity._deleteLocal.Count == 0
                                            || ! search.InterpolationListID(_listEntity._deleteLocal, ID, 0, _listEntity._deleteLocal.Count - 1, out listID))
                                            {
                                                _listEntity._changesDB._insertDB.Add(entityDB);

                                                if (right >= 0
                                                && ID <= _listEntity[right].ID)
                                                {
                                                    if( ! search.TryGetInsertListID(_listEntity, ID, left, right, out left))
                                                    {
                                                        NoticeError.SyncWithDBThrow();
                                                        return false;
                                                    }
                                                    _listEntity.InsertInternal(left, entityDB);
                                                }
                                                else
                                                    _listEntity.AddInternal(entityDB);
                                            }
                                            break;
                                    }
                                }
                                break;
                        }
                }
                if(_firstLoad)
                {
                    _firstLoad = false;
                    _listEntity._original = new ListSyncDB<T>(_listEntity);
                }
                _updateTimestamp = update_timestamp;
            }
            catch (Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
                return false;
            }
            return true;
        }

        internal bool HasChangesDB() => _listEntity._changesDB.HasChangesDB();
        internal Changes<T> GetChangesDB()
        {
            CheckDowntime();
            return _listEntity._changesDB.GetChangesDB();
        }

        internal bool HasConflicts() => _listEntity._conflicts._local.Count > 0;
        internal ConflictsWithDBOuter<T> GetConflicts() => _listEntity._conflicts.GetConflicts(_properties);
        internal void EliminateConflicts() => _listEntity._conflicts.EliminateConflicts();

        internal bool SyncCollectionWithTableDB()
        {
            var result = false;
            if( ! _init)
                NoticeError.SyncWithDBThrow();
            else
            {
                var deleteLocalID = GetIDs(_listEntity._deleteLocal);
                var updateLocal = _listEntity.Where(x => x.State == DomainObject.StateLocal.Update).OrderBy(x => x.ID);

                Changes<T> changes = new Changes<T>();
                var changesLocal = changes.SetChangesLocal(_properties, _columnNameList,
                    deleteLocalID,
                    updateLocal.ToList(),
                    _listEntity._insertLocal);

                var updateTimestamp = $"'{DateTime.UtcNow.AddTicks(-10).AddTicks(-1000):yyyy-MM-dd HH:mm:ss.ffffff}'";

                result = _IDAOEntity.SyncCollectionWithTableDB(typeof(T).FullName,
                    out DataSet[] dataSet, _updateTimestamp, GetIDs(_listEntity), _firstLoad, out bool hasConflicts,
                    changesLocal);

                if (hasConflicts)
                {
                    LoadDataFromDB(dataSet[0]);
                    return false;
                }

                if (dataSet[0] != null)
                    if( ! LoadDataFromDB(dataSet[0]))
                        return false;

                RemoveInsertsAndResetStates();

                if (dataSet[1] != null)
                    if( ! LoadDataFromDB(dataSet[1]))
                        return false;

                if (dataSet[0] != null || dataSet[1] != null)
                {
                    _listEntity._original = new ListSyncDB<T>(_listEntity);
                    _updateTimestamp = updateTimestamp;
                }
            }
            return result;
        }

        private void RemoveInsertsAndResetStates()
        {
            _listEntity._insertLocal.Clear();
            _listEntity._deleteLocal.Clear();

            int lastID = _listEntity.Count - 1;
            for (int e = lastID; e > 0; --e)
            {
                if (_listEntity[e].State == DomainObject.StateLocal.Insert)
                {
                    _listEntity._changesDB._deleteIdentifiersDB.Add(_listEntity[e].ID);
                    _listEntity.RemoveAt(e);
                    continue;
                }
                _listEntity[e].State = DomainObject.StateLocal.None;
                _listEntity[e]._original = null;
            }
        }

        internal void Rollback()
        {
            EliminateConflicts();

            _listEntity = _listEntity._original;

            var entities = _listEntity.Where(x => x.State == DomainObject.StateLocal.Update).ToArray();
            int countEntity = entities.Count();
            for (int e = 0; e < countEntity; ++e)
                entities[e] = (T)entities[e]._original;
        }

        internal void PrepareSaveRelations()
        {
            int countEntity = _listEntity.Count;
            for (int e = 0; e < countEntity; ++e)
                _listEntity[e]._needSaveRelations = true;
        }

        internal void PrepareRebuildCollections()
        {
            int countEntity = _listEntity.Count;
            for(int e = 0; e < countEntity; ++ e)
                _listEntity[e]._needBuildCollections = true;
        }

        private long[] GetIDs(IEnumerable<T> list)
        {
            T entity;
            List<long> listIDs = new List<long>();
            int countEntity = list.Count();
            for (int e = 0; e < countEntity; ++e)
            {
                entity = list.ElementAt(e);
                if(entity.State != DomainObject.StateLocal.Insert)
                    listIDs.Add(entity.ID);
            }
            return listIDs.ToArray();
        }
    }
}
