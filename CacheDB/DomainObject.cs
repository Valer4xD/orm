﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;

namespace CacheDB
{
    public class DomainObject
    {
        internal object _original;

        [DBTableColumnName("id")]
        public long ID { get; internal set; } = 0;

        internal int ListID;

        internal bool _isDelete = false;

        private StateLocal _state;
        internal StateLocal State
        {
            get { return _state; }
            set
            {
                if (value == StateLocal.Update
                && _state == StateLocal.Insert)
                    return;
                _state = value;
            }
        }
        public enum StateLocal : byte
        {
            None,
            Update,
            Insert
        }

        protected void TrySaveOriginalThis([CallerMemberName] string memberName = "")
        {
            if( ! _loadDataFromDB
            && State != StateLocal.Update)
            {
                _original = GetClone();
                State = StateLocal.Update;
            }
        }
        protected virtual object GetClone() => MemberwiseClone();

        internal bool _loadDataFromDB = false;

        public bool _needSaveRelations = false;
        public bool _needBuildCollections = true;

        public void SetCollection<TItem, TRelation>(ref ListSyncDB<TItem> collection, string thisForeignKeyName = null, string itemForeignKeyName = null)
            where TItem : DomainObject, new()
            where TRelation : DomainObject, new()
        {
            if (collection == null)
                if (!_needSaveRelations)
                    collection = new ListSyncDB<TItem>();

            if (collection != null)
            {
                if (_needBuildCollections)
                    BuildCollection<TItem, TRelation>(ref collection, thisForeignKeyName, itemForeignKeyName);

                if (_needSaveRelations)
                    SaveRelations<TItem, TRelation>(ref collection, thisForeignKeyName, itemForeignKeyName);
            }

            _needBuildCollections = false;
            _needSaveRelations = false;
        }

        private void TrySetForeignKeysName<TItem>(ref string thisForeignKeyName, ref string itemForeignKeyName)
        {
            if (string.IsNullOrWhiteSpace(thisForeignKeyName))
                thisForeignKeyName = $"{GetType().Name}ID";

            if (string.IsNullOrWhiteSpace(itemForeignKeyName))
                itemForeignKeyName = $"{typeof(TItem).Name}ID";
        }

        public void BuildCollection<TItem, TRelation>(ref ListSyncDB<TItem> collection, string thisForeignKeyName = null, string itemForeignKeyName = null)
            where TItem : DomainObject, new()
            where TRelation : DomainObject, new()
        {
            TrySetForeignKeysName<TItem>(ref thisForeignKeyName, ref itemForeignKeyName);

            collection = new ListSyncDB<TItem>();

            var allItems = CollectionFromTableDB<TItem>.GetInstance(/*Service.Service.GetCurrentUserID()*/).GetAll();
            var allRelations = CollectionFromTableDB<TRelation>.GetInstance(/*Service.Service.GetCurrentUserID()*/).GetAll();

            var thisForeignKeyPropertyInfo = typeof(TRelation).GetProperty(thisForeignKeyName);
            var itemForeignKeyPropertyInfo = typeof(TRelation).GetProperty(itemForeignKeyName);

            var thisRelations = allRelations.Where(x => ID == (long)thisForeignKeyPropertyInfo.GetValue(x, null));
            collection.AddRangeInternal
                (allItems.Where(x => thisRelations.Any(y => x.ID == (long)itemForeignKeyPropertyInfo.GetValue(y, null))));
        }

        public void SaveRelations<TItem, TRelation>(ref ListSyncDB<TItem> collection, string thisForeignKeyName = null, string itemForeignKeyName = null)
            where TItem : DomainObject, new()
            where TRelation : DomainObject, new()
        {
            TrySetForeignKeysName<TItem>(ref thisForeignKeyName, ref itemForeignKeyName);

            var deleteItems = collection._deleteLocal;
            var insertItems = collection._insertLocal;

            var allRelations = CollectionFromTableDB<TRelation>.GetInstance(/*Service.Service.GetCurrentUserID()*/).GetAll();

            int e,
                countDeleteItems = deleteItems.Count,
                countInsertItems = insertItems.Count;

            var thisForeignKeyPropertyInfo = typeof(TRelation).GetProperty(thisForeignKeyName);
            var itemForeignKeyPropertyInfo = typeof(TRelation).GetProperty(itemForeignKeyName);

            try
            {
                var thisRelations = allRelations.Where(x => ID == (long)thisForeignKeyPropertyInfo.GetValue(x, null));
                for (e = 0; e < countDeleteItems; ++e)
                    allRelations.Remove(thisRelations.SingleOrDefault(x =>
                        deleteItems[e].ID == (long)itemForeignKeyPropertyInfo.GetValue(x, null)));
            }
            catch(Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
                return;
            }

            TRelation relation;
            for (e = 0; e < countInsertItems; ++e)
            {
                relation = new TRelation();
                thisForeignKeyPropertyInfo.SetValue(relation, ID);
                itemForeignKeyPropertyInfo.SetValue(relation, insertItems[e].ID);
                allRelations.Add(relation);
            }

            for (e = 0; e < countInsertItems; ++e)
                insertItems[e].State = StateLocal.None;

            collection._deleteLocal.Clear();
            collection._insertLocal.Clear();
        }
    }
}
