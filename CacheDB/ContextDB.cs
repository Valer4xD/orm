﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CacheDB
{
    public class ContextDB
    {
        private IDAOEntity _IDAOEntity;

        private List<Type> _types = new List<Type>();
        private List<object> _instances = new List<object>();

        private List<MethodInfo> _getAll = new List<MethodInfo>(),
                                 _loadDataFromDB = new List<MethodInfo>(),
                                 _syncCollectionWithTableDB = new List<MethodInfo>(),
                                 _rollback = new List<MethodInfo>(),
                                 _hasChangesDB = new List<MethodInfo>(),
                                 _getChangesDB = new List<MethodInfo>(),
                                 _hasConflicts = new List<MethodInfo>(),
                                 _getConflicts = new List<MethodInfo>(),
                                 _eliminateConflicts = new List<MethodInfo>(),
                                 _prepareSaveRelations = new List<MethodInfo>(),
                                 _prepareRebuildCollections = new List<MethodInfo>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"> Подключение должно быть открытым. </param>
        /// <param name="typesIEnumerable"></param>
        public ContextDB(IDAOEntity @IDAOEntity, IEnumerable<Type> typesIEnumerable = null)
        {
            _IDAOEntity = @IDAOEntity ?? _IDAOEntity;
            if (_IDAOEntity == null)
                NoticeError.Throw(new ArgumentException());

            if(typesIEnumerable != null)
                AddRange(typesIEnumerable);
        }

        public List<Type> GetTypes() => _types.ToList();

        #region Add
        public void Add<T>() =>
                    Add(typeof(T));
        public void Add(Type type)
        {
            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return;
            }
            if(_types.Contains(type))
            {
                NoticeError.Throw(new ArgumentException());
                return;
            }

            _types.Add(type);

            var proxyType = typeof(CollectionFromTableDB<>);
            var genericClosedType = proxyType.MakeGenericType(type);

            var instance = genericClosedType.GetMethod("GetInstance", BindingFlags.Static | BindingFlags.NonPublic).Invoke(null, new object[] { 0L/*Service.Service.GetCurrentUserID()*/ });
            _instances.Add(instance);

            BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic;
            genericClosedType.GetMethod("Init", bindingFlags).Invoke(instance, new object[] { _IDAOEntity });

            _getAll.Add(genericClosedType.GetMethod("GetAll", bindingFlags));
            _loadDataFromDB.Add(genericClosedType.GetMethod("LoadDataFromDB", bindingFlags));
            _syncCollectionWithTableDB.Add(genericClosedType.GetMethod("SyncCollectionWithTableDB", bindingFlags));
            _rollback.Add(genericClosedType.GetMethod("Rollback", bindingFlags));
            _hasChangesDB.Add(genericClosedType.GetMethod("HasChangesDB", bindingFlags));
            _getChangesDB.Add(genericClosedType.GetMethod("GetChangesDB", bindingFlags));
            _hasConflicts.Add(genericClosedType.GetMethod("HasConflicts", bindingFlags));
            _getConflicts.Add(genericClosedType.GetMethod("GetConflicts", bindingFlags));
            _eliminateConflicts.Add(genericClosedType.GetMethod("EliminateConflicts", bindingFlags));
            _prepareSaveRelations.Add(genericClosedType.GetMethod("PrepareSaveRelations", bindingFlags));
            _prepareRebuildCollections.Add(genericClosedType.GetMethod("PrepareRebuildCollections", bindingFlags));
        }
        public void AddRange(IEnumerable<Type> typesIEnumerable)
        {
            if(typesIEnumerable == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return;
            }

            int count = typesIEnumerable.Count();
            for (int t = 0; t < count; ++t)
                Add(typesIEnumerable.ElementAt(t));
        }
        #endregion

        #region Remove
        public bool Remove<T>() =>
                    Remove(typeof(T));
        public bool Remove(Type type)
        {
            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }

            int t = _types.IndexOf(type);
            if (t >= 0)
            {
                _types.RemoveAt(t);
                _instances.RemoveAt(t);

                _getAll.RemoveAt(t);
                _loadDataFromDB.RemoveAt(t);
                _syncCollectionWithTableDB.RemoveAt(t);
                _rollback.RemoveAt(t);
                _hasChangesDB.RemoveAt(t);
                _getChangesDB.RemoveAt(t);
                _hasConflicts.RemoveAt(t);
                _getConflicts.RemoveAt(t);
                _eliminateConflicts.RemoveAt(t);
                _prepareSaveRelations.RemoveAt(t);
                _prepareRebuildCollections.RemoveAt(t);

                return true;
            }
            return false;
        }
        public bool RemoveRange(IEnumerable<Type> typesIEnumerable = null)
        {
            Type[] types = GetTypesSubsetOrAll(typesIEnumerable);

            int count = types.Length;
            for (int t = 0; t < count; ++t)
                if( ! Remove(types[t]))
                    return false;
            return true;
        }
        #endregion

        #region GetAll
        public bool GetAll<T>(out ListSyncDB<T> collection) where T : DomainObject, new() =>
                    GetAll(typeof(T), out collection);
        public bool GetAll<T>(Type type, out ListSyncDB<T> collection) where T : DomainObject, new()
        {
            collection = null;

            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }

            int t = _types.IndexOf(type);
            if (t >= 0)
            {
                collection = (ListSyncDB<T>)_getAll[t].Invoke(_instances[t], null);
                return true;
            }
            return false;
        }
        #endregion

        public bool LoadDataFromDB()
        {
            InvokeMethod(_prepareSaveRelations);
            if( ! IsSuccessInvokeMethod(_loadDataFromDB, new object[] { null }))
                return false;
            InvokeMethod(_prepareRebuildCollections);
            return true;
        }

        public bool SyncCollectionWithTableDB()
        {
            InvokeMethod(_prepareSaveRelations);
            if( ! IsSuccessInvokeMethod(_syncCollectionWithTableDB, null))
                return false;
            InvokeMethod(_prepareRebuildCollections);
            return true;
        }

        public void Rollback()
        {
            InvokeMethod(_rollback);
            InvokeMethod(_prepareRebuildCollections);
        }

        #region ChangesDB
        public bool HasChangesDB =>
                          GetTypesChangesDB().Count > 0;
        public List<Type> GetTypesChangesDB() => GetTypesMethodReturned(_hasChangesDB);
        public bool GetTypesChangesDB(out List<Type> types)
        {
            types = GetTypesChangesDB();

            return types.Count > 0;
        }
        public bool GetChangesDB<T>(out Changes<T> changesDB) where T : DomainObject, new() =>
                    GetChangesDB(typeof(T), out changesDB);
        public bool GetChangesDB<T>(Type type, out Changes<T> changesDB) where T : DomainObject, new()
        {
            changesDB = null;

            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }

            int t = _types.IndexOf(type);
            if (t >= 0)
            {
                changesDB = (Changes<T>)_getChangesDB[t].Invoke(_instances[t], null);
                return true;
            }
            return false;
        }
        #endregion

        #region Conflicts
        public bool HasConflicts =>
                          GetTypesHasConflicts().Count > 0;
        public List<Type> GetTypesHasConflicts() => GetTypesMethodReturned(_hasConflicts);
        public bool GetTypesHasConflicts(out List<Type> types)
        {
            types = GetTypesHasConflicts();

            return types.Count > 0;
        }
        public bool GetConflicts<T>(out ConflictsWithDBOuter<T> conflicts) where T : DomainObject, new() =>
                    GetConflicts(typeof(T), out conflicts);
        public bool GetConflicts<T>(Type type, out ConflictsWithDBOuter<T> conflicts) where T : DomainObject, new()
        {
            conflicts = null;

            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }

            int t = _types.IndexOf(type);
            if (t >= 0)
            {
                conflicts = (ConflictsWithDBOuter<T>)_getConflicts[t].Invoke(_instances[t], null);
                return true;
            }
            return false;
        }

        public bool EliminateConflicts<T>() =>
                    EliminateConflicts(typeof(T));
        public bool EliminateConflicts(Type type)
        {
            if(type == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }

            int t = _types.IndexOf(type);
            if (t >= 0)
            {
                _eliminateConflicts[t].Invoke(_instances[t], null);
                return true;
            }
            return false;
        }
        public bool EliminateConflictsRange(IEnumerable<Type> typesIEnumerable = null)
        {
            Type[] types = GetTypesSubsetOrAll(typesIEnumerable);

            int count = types.Length;
            for (int t = 0; t < count; ++t)
                if( ! EliminateConflicts(types[t]))
                    return false;
            return true;
        }
        #endregion

        #region Eliminates duplicate code
        private Type[] GetTypesSubsetOrAll(IEnumerable<Type> types)
        {
            if (types == null)
                return _types.ToArray();
            return types.ToArray();
        }

        private List<Type> GetTypesMethodReturned(List<MethodInfo> listMethodInfo)
        {
            List<Type> types = new List<Type>();
            int t,
                count = _types.Count();
            for (t = 0; t < count; ++t)
                if((bool)listMethodInfo[t].Invoke(_instances[t], null))
                    types.Add(_types[t]);
            return types;
        }

        private void InvokeMethod(List<MethodInfo> listMethodInfo)
        {
            int t,
                count = _types.Count();
            for (t = 0; t < count; ++t)
                listMethodInfo[t].Invoke(_instances[t], null);
        }

        private bool IsSuccessInvokeMethod(List<MethodInfo> listMethodInfo, object[] @params = null)
        {
            int t,
                count = _types.Count();
            for (t = 0; t < count; ++t)
                if( ! (bool)listMethodInfo[t].Invoke(_instances[t], @params))
                    return false;
            return true;
        }
        #endregion
    }
}
