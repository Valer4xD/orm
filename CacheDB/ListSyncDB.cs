﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace CacheDB
{
    public sealed class ListSyncDB<T> : IEnumerable<T>, IList<T> where T : DomainObject, new()
    {
        private List<T> _this = new List<T>();
        internal ListSyncDB<T> _original;
        internal List<T> _insertLocal = new List<T>(),
                         _deleteLocal = new List<T>();
        internal Changes<T> _changesDB = new Changes<T>();
        internal ConflictsWithDB<T> _conflicts = new ConflictsWithDB<T>();
        internal bool _loadDataFromDB = false;
        private SearchInCollection<T> search = new SearchInCollection<T>();

        internal ListSyncDB() {}
        internal ListSyncDB(ListSyncDB<T> previous) => _this.AddRange(new List<T>(previous._this));

        bool ICollection<T>.IsReadOnly => ((ICollection<T>)_this).IsReadOnly;

        #region Internal
        internal void AddInternal(T item) => _this.Add(item);
        internal void AddRangeInternal(IEnumerable<T> collection) => _this.AddRange(collection);
        internal void InsertInternal(int index, T item) => _this.Insert(index, item);
        internal void RemoveAtInternal(int index) => _this.RemoveAt(index);
        #endregion

        #region Insert
        /// <summary>
        /// Отмечает добавленные объекты или снимает отметку удалён с ранее загруженного объекта.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        private void CheckIns(T item, ref int index)
        {
            if (item == null
            || _loadDataFromDB)
            {
                NoticeError.SyncWithDBThrow();
                return;
            }

            if( ! _this.Contains(item))
            {
                if (_deleteLocal.Contains(item))
                    _deleteLocal.Remove(item);
                else
                {
                    item.State = DomainObject.StateLocal.Insert;
                    int count = _insertLocal.Count;
                    if(count > 0)
                        item.ID = -- _insertLocal[count - 1].ID;
                    _insertLocal.Add(item);
                }

                if(index == -1)
                    _this.Add(item);
                else
                    _this.Insert(index ++, item);
            }
        }

        public void Add(T item)
        {
            int index = -1;
            CheckIns(item, ref index);
        }
        public void AddRange(IEnumerable<T> collection)
        {
            if(collection == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return;
            }

            int index = -1;
            int count = collection.Count();
            for (int i = 0; i < count; ++i)
                CheckIns(collection.ElementAt(i), ref index);
        }
        public void Insert(int index, T item) => CheckIns(item, ref index);
        public void InsertRange(int index, IEnumerable<T> collection)
        {
            if(collection == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return;
            }

            int count = collection.Count();
            for (int i = 0; i < count; ++i)
                CheckIns(collection.ElementAt(i), ref index);
        }
        #endregion

        #region Delete
        private void TryDel(T item)
        {
            if (_loadDataFromDB)
            {
                NoticeError.SyncWithDBThrow();
                return;
            }

            if (item.State != DomainObject.StateLocal.Insert)
                if(_deleteLocal.Count > 0)
                {
                    search.TryGetInsertListID(_deleteLocal, item.ID, 0, _deleteLocal.Count - 1, out int listID);
                    _deleteLocal.Insert(listID, item);
                }
                else
                    _deleteLocal.Add(item);
            else
                _insertLocal.Remove(item);
        }

        public void Clear()
        {
            int thisCount = _this.Count;
            for(int i = 0; i < thisCount; ++ i)
                TryDel(_this[i]);
            _this.Clear();
        }
        public bool Remove(T item)
        {
            if (_this.Remove(item))
            {
                TryDel(item);
                return true;
            }
            return false;
        }
        public int RemoveAll(Predicate<T> match)
        {
            if(match != null)
            {
                var collection = _this.FindAll(match);
                int count = collection.Count;
                for (int i = 0; i < count; ++i)
                    TryDel(collection[i]);
            }
            return _this.RemoveAll(match);
        }
        public void RemoveAt(int index)
        {
            if(index >= 0
            && index < _this.Count)
                TryDel(_this[index]);
            _this.RemoveAt(index);
        }
        public void RemoveRange(int index, int count)
        {
            if(index >= 0
            && count >= 0)
                if (_this.Count - index >= count)
                {
                    int range = index + count;
                    for (int i = index; i <= range; ++i)
                        TryDel(_this[i]);
                }
            _this.RemoveRange(index, count);
        }
        #endregion

        #region Common with List<T>
        public T this[int index] { get { return _this[index]; } set { NoticeError.Throw(@"Попытка назначить элементу списка ""ListSyncDB<T>"" другой объект."); }}
        public int Count => _this.Count;
        public int Capacity { get { return _this.Capacity; } set { _this.Capacity = value; }}

        public ReadOnlyCollection<T> AsReadOnly() => _this.AsReadOnly();
        public int BinarySearch(int index, int count, T item, IComparer<T> comparer) => _this.BinarySearch(index, count, item, comparer);
        public int BinarySearch(T item) => _this.BinarySearch(item);
        public int BinarySearch(T item, IComparer<T> comparer) => _this.BinarySearch(item, comparer);
        public bool Contains(T item) => _this.Contains(item);
        public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter) => _this.ConvertAll<TOutput>(converter);
        public void CopyTo(T[] array, int arrayIndex) => _this.CopyTo(array, arrayIndex);
        public void CopyTo(int index, T[] array, int arrayIndex, int count) => _this.CopyTo(index, array, arrayIndex, count);
        public void CopyTo(T[] array) => _this.CopyTo(array);
        public bool Exists(Predicate<T> match) => _this.Exists(match);
        public T Find(Predicate<T> match) => _this.Find(match);
        public List<T> FindAll(Predicate<T> match) => _this.FindAll(match);
        public int FindIndex(Predicate<T> match) => _this.FindIndex(match);
        public int FindIndex(int startIndex, Predicate<T> match) => _this.FindIndex(startIndex, match);
        public int FindIndex(int startIndex, int count, Predicate<T> match) => _this.FindIndex(startIndex, count, match);
        public T FindLast(Predicate<T> match) => _this.FindLast(match);
        public int FindLastIndex(Predicate<T> match) => _this.FindLastIndex(match);
        public int FindLastIndex(int startIndex, Predicate<T> match) => _this.FindLastIndex(startIndex, match);
        public int FindLastIndex(int startIndex, int count, Predicate<T> match) => _this.FindLastIndex(startIndex, count, match);
        public void ForEach(Action<T> action) => _this.ForEach(action);
        public List<T> GetRange(int index, int count) => _this.GetRange(index, count);
        public int IndexOf(T item, int index, int count) => _this.IndexOf(item, index, count);
        public int IndexOf(T item, int index) => _this.IndexOf(item, index);
        public int IndexOf(T item) => _this.IndexOf(item);
        public int LastIndexOf(T item) => _this.LastIndexOf(item);
        public int LastIndexOf(T item, int index) => _this.LastIndexOf(item, index);
        public int LastIndexOf(T item, int index, int count) => _this.LastIndexOf(item, index, count);
        public void Reverse(int index, int count) => _this.Reverse(index, count);
        public void Reverse() => _this.Reverse();
        public void Sort(int index, int count, IComparer<T> comparer) => _this.Sort(index, count, comparer);
        public void Sort(Comparison<T> comparison) => _this.Sort(comparison);
        public void Sort() => _this.Sort();
        public void Sort(IComparer<T> comparer) => _this.Sort(comparer);
        public T[] ToArray() => _this.ToArray();
        public void TrimExcess() => _this.TrimExcess();
        public bool TrueForAll(Predicate<T> match) => _this.TrueForAll(match);
        #endregion

        #region GetEnumerator
        public List<T>.Enumerator GetEnumerator() => _this.GetEnumerator();
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => ((IEnumerable<T>)_this).GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)_this).GetEnumerator();
        #endregion
    }
}
