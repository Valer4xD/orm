﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CacheDB
{
    public class Changes<T> where T : DomainObject, new()
    {
        public List<long> DeleteIdentifiers;
        public List<T> Update,
                       Insert;

        internal List<long> _deleteIdentifiersDB = new List<long>();
        internal List<T> _updateDB = new List<T>(),
                         _insertDB = new List<T>();

        internal bool HasChangesDB()
        {
            if(_deleteIdentifiersDB.Count > 0
            || _updateDB.Count > 0
            || _insertDB.Count > 0)
                return true;
            return false;
        }
        
        internal Changes<T> GetChangesDB()
        {
            Changes<T> changesDB = new Changes<T>();

            DeleteIdentifiers = _deleteIdentifiersDB.ToList();
            Update = _updateDB.ToList();
            Insert = _insertDB.ToList();

            _deleteIdentifiersDB.Clear();
            _updateDB.Clear();
            _insertDB.Clear();

            return changesDB;
        }

        internal ChangesLocal SetChangesLocal(List<PropertyInfo> properties, List<string> _columnNameList,
            long[] deleteLocalID, List<T> updateLocal, List<T> insertLocal)
        {
            ChangesLocal changesLocal = new ChangesLocal();

            changesLocal.DeleteIdentifiers = deleteLocalID.ToArray();

            int c,
                countColumns = _columnNameList.Count();

            string columnName;
            Type type;
            for (c = 0; c < countColumns; ++c)
            {
                columnName = _columnNameList[c];
                type = properties[c].PropertyType;
                changesLocal.Update.Columns.Add(columnName, type);
                changesLocal.UpdateOriginal.Columns.Add(columnName, type);
                changesLocal.Insert.Columns.Add(columnName, type);

                if(columnName.ToLower() == "id")
                    changesLocal.ColumnWithID = c;
            }

            int countUpdate = updateLocal.Count(),
                countInsert = insertLocal.Count;
            object[] objArray = new object[countColumns];
            for (int u = 0; u < countUpdate; ++u)
            {
                for (c = 0; c < countColumns; ++c)
                    objArray[c] = properties[c].GetValue(updateLocal[u]);
                changesLocal.Update.Rows.Add(objArray);

                for (c = 0; c < countColumns; ++c)
                    objArray[c] = properties[c].GetValue(updateLocal[u]._original);
                changesLocal.UpdateOriginal.Rows.Add(objArray);
            }
            for (int u = 0; u < countInsert; ++u)
            {
                for (c = 0; c < countColumns; ++c)
                    objArray[c] = properties[c].GetValue(insertLocal[u]);
                changesLocal.Insert.Rows.Add(objArray);
            }

            return changesLocal;
        }
    }
}
