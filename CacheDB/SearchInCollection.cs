﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CacheDB
{
    public class SearchInCollection<T> where T : DomainObject
    {
        private const int _notFound = -1;

        public bool InterpolationListID(IEnumerable<T> list, long ID, int left, int right, out int listID)
        {
            int leftBackup = left,
                rightBackup = right;
            var result = InterpolationListID(list, ID, ref left, ref right, out listID);
            left = leftBackup;
            right = rightBackup;
            return result;
        }

        public bool InterpolationListID(IEnumerable<T> list, long ID, ref int left, ref int right, out int listID)
        {
            listID = _notFound;

            if(list == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }
            int count = list.Count();
            if (count == 0
            || left < 0 || left >= count
            || right < 0 || right >= count
            || left > right)
            {
                NoticeError.Throw(new ArgumentException());
                return false;
            }

            if (count == 1)
                if (ID == list.ElementAt(0).ID)
                {
                    listID = 0;
                    return true;
                }
                else
                    return false;

            bool found = false;
            long listCurrentID,
                 listLeftID = list.ElementAt(left).ID,
                 listRightID = list.ElementAt(right).ID;

            while( ! found && (listLeftID <= ID) && (listRightID >= ID))
            {
                listID = (int)(left + ((ID - listLeftID) * (right - left)) / (listRightID - listLeftID));
                listCurrentID = list.ElementAt(listID).ID;
                if (listCurrentID < ID)
                {
                    left = listID + 1;
                    listLeftID = list.ElementAt(left).ID;
                }
                else
                    if (listCurrentID > ID)
                    {
                        right = listID - 1;
                        listRightID = list.ElementAt(right).ID;
                    }
                    else
                        found = true;
            }
            return found;
        }

        public bool TryGetInsertListID(IEnumerable<T> list, long ID, int left, int right, out int listID)
        {
            int leftTemp = left,
                rightTemp = right;
            if( ! InterpolationListID(list, ID, ref leftTemp, ref rightTemp, out listID))
            {
                if (list.ElementAt(leftTemp).ID > ID)
                    listID = leftTemp;
                else
                    listID = rightTemp + 1;
                return true;
            }
            return false;
        }
    }
}
