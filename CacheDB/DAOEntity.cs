﻿using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CacheDB
{
    public class DAOEntity : IDAOEntity
    {
        private static ConcurrentDictionary<string, DAOTypeInitSave> _typeInitDictionary
            = new ConcurrentDictionary<string, DAOTypeInitSave>();
        private static object _syncRootInit = new object();

        private const string _updateTimestampSQLVar = "@update_timestamp",
                             _tableOldSQLVar = "@table_old";

        public enum TypeCache : byte
        {
            NotCache,
            TableCache,
            RowCache
        }

        private static NpgsqlConnection _connection;
        private static NpgsqlTransaction _transaction;

        public NpgsqlConnection Connection
        {
            get
            {
                if (_connection == null)
                    NoticeError.Throw(new NullReferenceException());
                return _connection;
            }
            set
            {
                _connection = value ?? _connection;
                if (_connection == null)
                    NoticeError.Throw(new NullReferenceException());
            }
        }

        private static int _ID;
        private static object _syncRootAddInQueue = new object(),
                              _syncRootReadQueue = new object();
        private static ConcurrentQueue<int> _queue = new ConcurrentQueue<int>();

        static bool FirstInQueue(int ID)
        {
            lock(_syncRootReadQueue)
            {
                _queue.TryPeek(out int firstIDInQueue);
                if (firstIDInQueue == ID)
                    return true;
            }
            return false;
        }

        private bool TransactionBegin()
        {
            int ID;
            lock(_syncRootAddInQueue)
            {
                ID = _ID ++;
                _queue.Enqueue(ID);
            }
            while( ! FirstInQueue(ID)) {}

            try
            {
                _transaction = Connection.BeginTransaction(IsolationLevel.RepeatableRead);
                return true;
            }
            catch(Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
            }
            return false;
        }

        private bool TransactionCommit()
        {
            try
            {
                _transaction.Commit();
                return true;
            }
            catch(Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
            }
            return false;
        }

        private void TransactionRollback()
        {
            try
            {
                _transaction.Rollback();
            }
            catch(Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
            }
        }

        private void TransactionDispose()
        {
            if(_transaction != null)
                _transaction.Dispose();

            lock(_syncRootReadQueue)
                lock(_syncRootAddInQueue)
                    _queue.TryDequeue(out int firstIDInQueue);
        }

        public bool SyncCollectionWithTableDB(string typeFullName,
            out DataSet[] dataSet, string update_timestamp, long[] allLocalID, bool _firstLoad, out bool hasConflicts,
            ChangesLocal changesLocal)
        {
            dataSet = new DataSet[2];
            hasConflicts = false;
            try
            {
                if(TransactionBegin()
                && (changesLocal.Update.Rows.Count == 0 || AdapterFill(typeFullName, out dataSet[0], update_timestamp, allLocalID, _firstLoad))
                && ! (hasConflicts = HasConflicts(
                    typeFullName, dataSet[0], changesLocal.Update, changesLocal.UpdateOriginal, changesLocal.ColumnWithID))
                && SaveChanges(typeFullName, changesLocal)
                && (changesLocal.Insert.Rows.Count == 0 || AdapterFill(typeFullName, out dataSet[1], update_timestamp, allLocalID, _firstLoad))
                && TransactionCommit())
                    return true;
                else
                    TransactionRollback();
            }
            finally
            {
                TransactionDispose();
            }
            return false;
        }

        public bool HasConflicts(string typeFullName, DataSet dataSet, DataTable update, DataTable updateOriginal, int columnWithID)
        {
            if(dataSet == null)
                return false;

            int countRowsDB = dataSet.Tables[0].Rows.Count;

            if(update.Rows.Count == 0
            || countRowsDB == 0)
                return false;

            var typeCache = GetTypeCache(typeFullName);
            int r,
                c,
                countColumns = update.Columns.Count;
            DataRow row;
            long ID;
            SearchInDataTable search = new SearchInDataTable();
            int rowID;

            switch(typeCache)
            {
                case TypeCache.NotCache:
                case TypeCache.TableCache:
                    for (r = 0; r < countRowsDB; ++r)
                    {
                        row = dataSet.Tables[0].Rows[r];
                        ID = (long)row["id"];
                        if(search.InterpolationRowID(update, columnWithID, ID, 0, update.Rows.Count - 1, out rowID))
                            for (c = 0; c < countColumns; ++c)
                                if(row.ItemArray[c].ToString() != updateOriginal.Rows[rowID][c].ToString()
                                && row.ItemArray[c].ToString() != update.Rows[rowID][c].ToString())
                                    return true;
                    }
                    break;
                case TypeCache.RowCache:
                    for (r = 0; r < countRowsDB; ++r)
                    {
                        row = dataSet.Tables[0].Rows[r];
                        if((CollectionFromTableDB<DomainObject>.StateRowTableDB)Convert.ToByte(row["state"])
                            == CollectionFromTableDB<DomainObject>.StateRowTableDB.Update)
                        {
                            ID = (long)row["id"];
                            if(search.InterpolationRowID(update, columnWithID, ID, 0, update.Rows.Count - 1, out rowID))
                                for (c = 0; c < countColumns; ++c)
                                    if(row.ItemArray[c].ToString() != updateOriginal.Rows[rowID][c].ToString()
                                    && row.ItemArray[c].ToString() != update.Rows[rowID][c].ToString())
                                        return true;
                        }
                    }
                    break;
            }
            return false;
        }

        public bool AdapterFill(string typeFullName, out DataSet dataSet, string update_timestamp, long[] allLocalID, bool _firstLoad)
        {
            dataSet = new DataSet();
            try
            {
                if( ! _typeInitDictionary.TryGetValue(typeFullName, out DAOTypeInitSave @DAOTypeInitSave))
                {
                    NoticeError.Throw(new InvalidOperationException());
                    return false;
                }

                var adapter = new NpgsqlDataAdapter(@DAOTypeInitSave._textSelectCommandSQLFullLoad, Connection);

                if( ! _firstLoad)
                {
                    AdapterSetTypeCache(@DAOTypeInitSave, ref adapter);
                    SetSelectCommandParameters(@DAOTypeInitSave._typeCache, ref adapter, update_timestamp, allLocalID);
                }

                adapter.Fill(dataSet);
                return true;
            }
            catch(Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
            }
            return false;
        }

        private void AdapterSetTypeCache(DAOTypeInitSave @DAOTypeInitSave, ref NpgsqlDataAdapter adapter)
        {
            switch (@DAOTypeInitSave._typeCache)
            {
                case TypeCache.TableCache:
                    adapter.SelectCommand.CommandText = @DAOTypeInitSave._textSelectCommandSQLTableCache;
                    adapter.SelectCommand.Parameters.Add(_updateTimestampSQLVar, NpgsqlDbType.Unknown);
                    break;
                case TypeCache.RowCache:
                    adapter.SelectCommand.CommandText = @DAOTypeInitSave._textSelectCommandSQLRowCache;
                    adapter.SelectCommand.Parameters.Add(_updateTimestampSQLVar, NpgsqlDbType.Unknown);
                    adapter.SelectCommand.Parameters.Add(_tableOldSQLVar, NpgsqlDbType.Array | NpgsqlDbType.Bigint);
                    break;
            }
        }

        private void SetSelectCommandParameters(
            TypeCache typeCache, ref NpgsqlDataAdapter adapter, string update_timestamp, long[] allLocalID)
        {
            switch (typeCache)
            {
                case TypeCache.TableCache:
                    adapter.SelectCommand.Parameters[_updateTimestampSQLVar].Value = update_timestamp;
                    break;
                case TypeCache.RowCache:
                    adapter.SelectCommand.Parameters[_tableOldSQLVar].Value = allLocalID;
                    adapter.SelectCommand.Parameters[_updateTimestampSQLVar].Value = update_timestamp;
                    break;
            }
        }

        public bool SaveChanges(string typeFullName, ChangesLocal changesLocal)
        {
            if( ! _typeInitDictionary.TryGetValue(typeFullName, out DAOTypeInitSave @DAOTypeInitSave))
            {
                NoticeError.Throw(new InvalidOperationException());
                return false;
            }

            var commandSQL = new NpgsqlCommand();
            commandSQL.Connection = Connection;

            commandSQL.CommandText = $@"
            DO
            $DO$
	            BEGIN
                    {GetTextDeleteCommand(changesLocal.DeleteIdentifiers.ToArray(), @DAOTypeInitSave._textDeleteCommandSQLPrefix)}
                    {GetTextUpdateCommand(changesLocal.Update, changesLocal.ColumnWithID, @DAOTypeInitSave)}
                    {GetTextInsertCommand(changesLocal.Insert, changesLocal.ColumnWithID, @DAOTypeInitSave)}
	            END
            $DO$";

            try
            {
                commandSQL.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
            }
            return false;
        }

        private string GetTextDeleteCommand(long[] values, string textDeleteCommandSQLPrefix)
        {
            if (values.Count() != values.Distinct().Count())
            {
                NoticeError.SyncWithDBThrow();
                return string.Empty;
            }

            var result = new StringBuilder(textDeleteCommandSQLPrefix);
            var separator = ", ";
            int count = values.Length;
            for (int i = 0; i < count; ++i)
                result.Append($"{values[i]}{separator}");
            if(count > 0)
                result.Remove(result.Length - separator.Length, separator.Length);
            return $"{result}]::BIGINT[]);";
        }

        private string GetTextUpdateCommand(DataTable table, int columnWithID, DAOTypeInitSave @DAOTypeInitSave)
        {
            if ( ! HasRepeatsOrIncorrectID(table, columnWithID) && table.Rows.Count > 0)
            {
                var result = new StringBuilder(@DAOTypeInitSave._textUpdateCommandSQLPrefix);
                result.Append(
                    $@"{GetCommandParameters(DomainObject.StateLocal.Update, table, @DAOTypeInitSave)})
                        as new({GetNamesColumnsTableDB(@DAOTypeInitSave._columnNameList)})
                            WHERE old.id = new.id;");
                return result.ToString();
            }
            return string.Empty;
        }

        private string GetTextInsertCommand(DataTable table, int columnWithID, DAOTypeInitSave @DAOTypeInitSave)
        {
            return ( ! HasRepeatsOrIncorrectID(table, columnWithID) && table.Rows.Count > 0)
                ? (@DAOTypeInitSave._textInsertCommandSQLPrefix + $"{GetCommandParameters(DomainObject.StateLocal.Insert, table, @DAOTypeInitSave)};")
                : string.Empty;
        }

        private bool HasRepeatsOrIncorrectID(DataTable table, int columnWithID)
        {
            int countRows = table.Rows.Count;
            List<long> IDs = new List<long>();
            try
            {
                for (int r = 0; r < countRows; ++r)
                    IDs.Add((long)table.Rows[r][columnWithID]);
            }
            catch
            {
                NoticeError.SyncWithDBThrow();
                return true;
            }
            if (countRows != IDs.Distinct().Count())
            {
                NoticeError.SyncWithDBThrow();
                return true;
            }
            return false;
        }

        private string GetCommandParameters(
            DomainObject.StateLocal typeState,
            DataTable table,
            DAOTypeInitSave @DAOTypeInitSave)
        {
            var columnTypeList = @DAOTypeInitSave._columnTypeList;

            var separator = ", ";
            int countRows = table.Rows.Count,
                countColumns = @DAOTypeInitSave._columnNameList.Count,
                c;
            var values = new StringBuilder();
            for (int r = 0; r < countRows; ++r)
            {
                values.Append("(");
                for (c = 0; c < countColumns; ++c)
                {
                    switch(typeState)
                    {
                        case DomainObject.StateLocal.Insert:
                            if (table.Columns[c].ToString().ToLower() == "id")
                                continue;
                            values.Append($"'{table.Rows[r][c]}'{separator}");
                            break;
                        case DomainObject.StateLocal.Update:
                            values.Append($"'{table.Rows[r][c]}'::{columnTypeList[c]}{separator}");
                            break;
                    }
                }
                values.Remove(values.Length - separator.Length, separator.Length);
                values.Append($"){separator}");
            }
            values.Remove(values.Length - separator.Length, separator.Length);

            return values.ToString();
        }

        public bool Init(string typeFullName, string tableName, List<string> columnNameList)
        {
            if( ! _typeInitDictionary.ContainsKey(typeFullName))
                lock (_syncRootInit)
                    if( ! _typeInitDictionary.ContainsKey(typeFullName))
                    {
                        if( ! InitTypeCache(tableName, out TypeCache typeCache)
                        ||  ! InitColumnTypeList(tableName, typeCache, columnNameList, out List<string> columnTypeList))
                            return false;

                        BuildTextSelectCommandSQLFullLoad(tableName, columnNameList, out string textSelectCommandSQLFullLoad);
                        BuildTextSelectCommandSQLTypeCache(tableName, typeCache, textSelectCommandSQLFullLoad, columnNameList,
                            out string textSelectCommandSQLTableCache, out string textSelectCommandSQLRowCache);

                        BuildTextDeleteCommandSQLPrefix(tableName, out string textDeleteCommandSQLPrefix);
                        BuildTextUpdateCommandSQL(tableName, columnNameList, out string textUpdateCommandSQLPrefix);
                        BuildTextInsertCommandSQLPrefix(tableName, columnNameList, out string textInsertCommandSQLPrefix);

                        DAOTypeInitSave @DAOTypeInitSave = new DAOTypeInitSave(
                            columnNameList,
                            columnTypeList,                
                                typeCache,
                                textSelectCommandSQLFullLoad,
                                textSelectCommandSQLTableCache,
                                textSelectCommandSQLRowCache,
                                    textDeleteCommandSQLPrefix,
                                    textUpdateCommandSQLPrefix,
                                    textInsertCommandSQLPrefix);
                        _typeInitDictionary.TryAdd(typeFullName, @DAOTypeInitSave);
                    }
            return true;
        }

        private bool InitTypeCache(string tableName, out TypeCache typeCache)
        {
            typeCache = TypeCache.NotCache;

            var getDBTableColumnsNamesQueryText =
                $"SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{tableName}'";
            var dataSet = new DataSet();
            try
            {
                var adapter = new NpgsqlDataAdapter(getDBTableColumnsNamesQueryText, Connection);
                adapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
                return false;
            }
            if (dataSet.Tables?.Count != 1)
            {
                NoticeError.SyncWithDBThrow();
                return false;
            }
            var rowCache = false;
            int countRows = dataSet.Tables[0].Rows.Count;
            DataRow row;
            for (int r = 0; r < countRows; ++ r)
            {
                row = dataSet.Tables[0].Rows[r];
                if (row.ItemArray.Length == 1)
                    if (row.ItemArray[0].ToString() == "update_timestamp")
                    {
                        rowCache = true;
                        break;
                    }
            }
            if (rowCache)
                typeCache = TypeCache.RowCache;
            else
            {
                var isExistsDBTableUpdateTimestampQueryText =
                    $"SELECT table_name FROM update_timestamp WHERE table_name = '{tableName}'";
                dataSet = new DataSet();
                try
                {
                    var adapter = new NpgsqlDataAdapter(isExistsDBTableUpdateTimestampQueryText, Connection);
                    adapter.Fill(dataSet);
                }
                catch (Exception ex)
                {
                    NoticeError.SyncWithDBThrow(ex);
                    return false;
                }
                if (dataSet.Tables?.Count == 1
                && dataSet.Tables[0].Rows.Count == 1
                && dataSet.Tables[0].Rows[0].ItemArray.Length == 1
                && dataSet.Tables[0].Rows[0].ItemArray[0].ToString() == tableName)
                    typeCache = TypeCache.TableCache;
            }
            return true;
        }

        private bool InitColumnTypeList
            (string tableName, TypeCache typeCache, List<string> columnNameList, out List<string> columnTypeList)
        {
            columnTypeList = new List<string>();

            if (typeCache != TypeCache.RowCache)
                return true;

            var dataSet = new DataSet();
            int countColumnsTableDB;

            var getTypeColumnTableQueryText =
                $"SELECT column_name, data_type FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '{tableName}'";
            try
            {
                var adapter = new NpgsqlDataAdapter(getTypeColumnTableQueryText, Connection);
                adapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                NoticeError.SyncWithDBThrow(ex);
                return false;
            }

            if(dataSet.Tables?.Count == 1
            && dataSet.Tables[0].Columns.Count == 2)
                countColumnsTableDB = dataSet.Tables[0].Rows.Count;
            else
            {
                NoticeError.SyncWithDBThrow();
                return false;
            }

            int c;
            bool foundType;
            int columnNameCount = columnNameList.Count;
            for (int p = 0; p < columnNameCount; ++p)
            {
                foundType = false;
                for (c = 0; c < countColumnsTableDB; ++c)
                    if (columnNameList[p] == dataSet.Tables[0].Rows[c].ItemArray[0].ToString())
                    {
                        columnTypeList.Add(dataSet.Tables[0].Rows[c].ItemArray[1].ToString());
                        foundType = true;
                        break;
                    }
                if( ! foundType)
                {
                    NoticeError.SyncWithDBThrow();
                    return false;
                }
            }
            return true;
        }

        private void BuildTextSelectCommandSQLFullLoad
            (string tableName, List<string> columnNameList, out string textSelectCommandSQLFullLoad)
        {
            var textSelectCommandSQLFullLoadSB = new StringBuilder("SELECT ");
            textSelectCommandSQLFullLoadSB.Append(GetNamesColumnsTableDB(columnNameList));
            textSelectCommandSQLFullLoadSB.Append($" FROM {tableName} ORDER BY id;");
            textSelectCommandSQLFullLoad = textSelectCommandSQLFullLoadSB.ToString();
        }

        private void BuildTextSelectCommandSQLTypeCache(
            string tableName,
            TypeCache typeCache,
            string textSelectCommandSQLFullLoad,
            List<string> columnNameList,
            out string textSelectCommandSQLTableCache,
            out string textSelectCommandSQLRowCache)
        {
            textSelectCommandSQLTableCache = null;
            textSelectCommandSQLRowCache = null;

            switch (typeCache)
            {
                case TypeCache.TableCache:

                    textSelectCommandSQLTableCache = $@"
                        {textSelectCommandSQLFullLoad} FETCH FIRST
		                    (case when
			                    ((SELECT update_timestamp FROM update_timestamp WHERE table_name = '{tableName}') > {_updateTimestampSQLVar})
		                    THEN
			                    NULL
		                    ELSE
			                    0
		                    END)
	                    ROWS ONLY;";
                    break;
                case TypeCache.RowCache:
                    var textSelectCommandSQLRowCacheSB = new StringBuilder("((SELECT ");

                    var namesColumnsTableDB = new StringBuilder();
                    string columnName;
                    var separator = ", ";
                    int countColumns = columnNameList.Count;
                    for (int c = 0; c < countColumns; ++c)
                    {
                        columnName = columnNameList[c];
                        if (columnName == "id")
                            namesColumnsTableDB.Append($"old.{columnName}{separator}");
                        else
                            namesColumnsTableDB.Append($"NULL AS {columnName}{separator}");
                    }
                    namesColumnsTableDB.Append("0 AS state");
                    textSelectCommandSQLRowCacheSB.Append(namesColumnsTableDB);
                    textSelectCommandSQLRowCacheSB.Append($@"
                        FROM {tableName}
			                RIGHT JOIN
				                (SELECT * FROM UNNEST({_tableOldSQLVar}) AS id ORDER BY id) AS old
					                ON {tableName}.id = old.id
						                WHERE {tableName}.id IS NULL)
                    UNION
                    (SELECT ");
                    textSelectCommandSQLRowCacheSB.Append($"{GetNamesColumnsTableDB(columnNameList, withoutID: true)}, updated.id, 1 AS state");
                    namesColumnsTableDB = GetNamesColumnsTableDB(columnNameList);
                    textSelectCommandSQLRowCacheSB.Append($@"
                        FROM (SELECT {namesColumnsTableDB} FROM {tableName} WHERE update_timestamp > {_updateTimestampSQLVar}) AS updated
                            LEFT JOIN (SELECT id FROM {tableName} WHERE NOT(id = ANY({_tableOldSQLVar}))) AS added
                            	ON updated.ID = added.id WHERE added.id IS NULL)
                    UNION
                    (SELECT {namesColumnsTableDB}, 2 AS state FROM {tableName} WHERE NOT(id = ANY({_tableOldSQLVar}))))
                    ORDER BY id;");
                    textSelectCommandSQLRowCache = textSelectCommandSQLRowCacheSB.ToString();
                    break;
            }
        }

        private void BuildTextDeleteCommandSQLPrefix(string tableName, out string textDeleteCommandSQLPrefix) =>
            textDeleteCommandSQLPrefix = $"DELETE FROM {tableName} WHERE id = ANY(ARRAY[";

        private void BuildTextUpdateCommandSQL(string tableName, List<string> columnNameList, out string textUpdateCommandSQLPrefix)
        {
            var textUpdateCommandSQLSB = new StringBuilder($@"UPDATE {tableName} AS old SET ");

            string columnName;
            var separator = ", ";
            int countColumns = columnNameList.Count;
            for (int c = 0; c < countColumns; ++c)
            {
                columnName = columnNameList[c];
                if (columnName != "id")
                    textUpdateCommandSQLSB.Append($"{columnName} = new.{columnName}{separator}");
            }
            textUpdateCommandSQLSB.Remove(textUpdateCommandSQLSB.Length - separator.Length, separator.Length);

            textUpdateCommandSQLSB.Append($@" FROM (VALUES ");

            textUpdateCommandSQLPrefix = textUpdateCommandSQLSB.ToString();
        }

        private void BuildTextInsertCommandSQLPrefix
            (string tableName, List<string> columnNameList, out string textInsertCommandSQLPrefix) =>
                textInsertCommandSQLPrefix = $"INSERT INTO {tableName}({GetNamesColumnsTableDB(columnNameList, withoutID: true)}) VALUES ";

        private StringBuilder GetNamesColumnsTableDB(List<string> columnNameList, bool withoutID = false)
        {
            var namesColumnsTableDB = new StringBuilder();
            string columnName;
            var separator = ", ";
            int countColumns = columnNameList.Count;
            for (int c = 0; c < countColumns; ++c)
            {
                columnName = columnNameList[c];
                if (withoutID && columnName == "id")
                    continue;
                namesColumnsTableDB.Append($"{columnName}{separator}");
            }
            namesColumnsTableDB.Remove(namesColumnsTableDB.Length - separator.Length, separator.Length);
            return namesColumnsTableDB;
        }

        public TypeCache GetTypeCache(string type)
        {
            if( ! _typeInitDictionary.TryGetValue(type, out DAOTypeInitSave @DAOTypeInitSave))
            {
                NoticeError.Throw(new InvalidOperationException());
                return TypeCache.NotCache;
            }

            return @DAOTypeInitSave._typeCache;
        }
    }
}
