﻿using System.Collections.Generic;

namespace CacheDB
{
    public class ConflictsWithDBOuter<T> where T : DomainObject, new()
    {
        public List<T> Original = new List<T>(),
                       Local = new List<T>(),
                       Other = new List<T>();

        internal ConflictsWithDBOuter() {}
    }
}
