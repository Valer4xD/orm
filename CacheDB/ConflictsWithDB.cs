﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CacheDB
{
    internal class ConflictsWithDB<T> where T : DomainObject, new()
    {
        internal List<T> _original = new List<T>(),
                         _local = new List<T>(),
                         _other = new List<T>();

        internal ConflictsWithDB() {}

        internal void Add(T original, T local, T other)
        {
            _original.Add(original);
            _local.Add(local);
            _other.Add(other);
        }

        internal void Clear()
        {
            _original.Clear();
            _local.Clear();
            _other.Clear();
        }

        internal ConflictsWithDBOuter<T> GetConflicts(IEnumerable<PropertyInfo> properties)
        {
            var conflictsWithDBOuter = new ConflictsWithDBOuter<T>();
            int c,
                countColumns = properties.Count(),
                countConflicts = _local.Count;
            for(int e = 0; e < countConflicts; ++ e)
            {
                conflictsWithDBOuter.Original.Add(new T());
                conflictsWithDBOuter.Other.Add(new T());
                for (c = 0; c < countColumns; ++c)
                {
                    properties.ElementAt(c).SetValue(conflictsWithDBOuter.Original[e], properties.ElementAt(c).GetValue(_original[e]));
                    properties.ElementAt(c).SetValue(conflictsWithDBOuter.Other[e], properties.ElementAt(c).GetValue(_other[e]));
                }
            }
            conflictsWithDBOuter.Local = _local;
            return conflictsWithDBOuter;
        }

        internal void EliminateConflicts()
        {
            int count = _local.Count;
            for (int e = 0; e < count; ++ e)
                _local[e]._original = _other[e];
        }
    }
}
