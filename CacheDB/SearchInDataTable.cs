﻿using System;
using System.Data;

namespace CacheDB
{
    public class SearchInDataTable
    {
        private const int _notFound = -1;

        public bool InterpolationRowID(DataTable table, int ColumnWithID, long ID, int left, int right, out int rowID)
        {
            rowID = _notFound;

            if(table == null)
            {
                NoticeError.Throw(new NullReferenceException());
                return false;
            }
            int count = table.Rows.Count;
            if (count == 0
            || left < 0 || left >= count
            || right < 0 || right >= count
            || left > right)
            {
                NoticeError.Throw(new ArgumentException());
                return false;
            }

            if (count == 1)
                if (ID == (long)table.Rows[0][ColumnWithID])
                {
                    rowID = 0;
                    return true;
                }
                else
                    return false;

            bool found = false;
            long tableCurrentID,
                 tableLeftID = (long)table.Rows[left][ColumnWithID],
                 tableRightID = (long)table.Rows[right][ColumnWithID];

            while( ! found && (tableLeftID <= ID) && (tableRightID >= ID))
            {
                rowID = (int)(left + ((ID - tableLeftID) * (right - left)) / (tableRightID - tableLeftID));
                tableCurrentID = (long)table.Rows[rowID][ColumnWithID];
                if (tableCurrentID < ID)
                {
                    left = rowID + 1;
                    tableLeftID = (long)table.Rows[left][ColumnWithID];
                }
                else
                    if (tableCurrentID > ID)
                    {
                        right = rowID - 1;
                        tableRightID = (long)table.Rows[right][ColumnWithID];
                    }
                    else
                        found = true;
            }
            return found;
        }
    }
}
