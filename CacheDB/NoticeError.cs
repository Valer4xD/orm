﻿using System;

namespace CacheDB
{
    internal static class NoticeError
    {
        internal static void SyncWithDBThrow(Exception ex = null) =>
            Throw($"При синхронизации с базой данных произошла ошибка.{(ex != null ? $" Подробности: {ex.Message}" : string.Empty)}");

        internal static void Throw(string textSuffix) => throw new Exception($"Обратитесь к разработчикам программы. {textSuffix}");

        internal static void Throw(Exception ex) => throw ex;
    }
}
