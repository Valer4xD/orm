﻿using System.Data;

namespace CacheDB
{
    public class ChangesLocal
    {
        public int ColumnWithID;

        public long[] DeleteIdentifiers;
        public DataTable Update = new DataTable("DataTable"),
                         UpdateOriginal = new DataTable("DataTable"),
                         Insert = new DataTable("DataTable");
    }
}
