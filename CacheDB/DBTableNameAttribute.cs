﻿using System;

namespace CacheDB
{
    public class DBTableNameAttribute : Attribute
    {
        public string TableName { get; private set; }
        public DBTableNameAttribute(string tableName)
        {
            if(string.IsNullOrWhiteSpace(tableName))
            {
                NoticeError.Throw(new ArgumentException());
                return;
            }
            TableName = tableName;
        }
    }
}
