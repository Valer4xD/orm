﻿using System.Collections.Generic;
using System.Data;
using System.ServiceModel;

namespace CacheDB
{
    [ServiceContract]
    public interface IDAOEntity
    {
        [OperationContract]
        bool Init(string typeFullName, string tableName, List<string> columnNameList);

        [OperationContract]
        bool AdapterFill(string typeFullName, out DataSet dataSet, string update_timestamp, long[] allLocalID, bool _firstLoad);

        [OperationContract]
        bool SyncCollectionWithTableDB(string typeFullName,
            out DataSet[] dataSet, string update_timestamp, long[] allLocalID, bool _firstLoad, out bool hasConflicts,
            ChangesLocal changesLocal);

        [OperationContract]
        DAOEntity.TypeCache GetTypeCache(string typeFullName);
    }
}
