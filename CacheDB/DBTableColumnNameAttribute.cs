﻿using System;

namespace CacheDB
{
    public class DBTableColumnNameAttribute : Attribute
    {
        public string ColumnName { get; private set; }
        public DBTableColumnNameAttribute(string columnName)
        {
            if(string.IsNullOrWhiteSpace(columnName))
            {
                NoticeError.Throw(new ArgumentException());
                return;
            }
            ColumnName = columnName;
        }
    }
}
