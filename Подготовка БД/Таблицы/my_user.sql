-- public.my_user definition

-- Drop table

-- DROP TABLE public.my_user;

CREATE TABLE public.my_user (
	id bigserial NOT NULL,
	firstname varchar(64) NOT NULL,
	surname varchar(64) NOT NULL,
	secondname varchar(64) NULL,
	update_timestamp timestamp NOT NULL,
	CONSTRAINT my_user_pk PRIMARY KEY (id)
);

-- Table Triggers

create trigger my_user_update_timestamp_column_trigger before
insert
    or
update
    of firstname,
    surname,
    secondname on
    my_user for each row execute procedure my_user_update_timestamp_column_function();

-- Permissions

ALTER TABLE public.my_user OWNER TO pgroot;
GRANT ALL ON TABLE public.my_user TO pgroot;
