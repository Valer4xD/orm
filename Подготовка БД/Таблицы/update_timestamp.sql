-- public.update_timestamp definition

-- Drop table

-- DROP TABLE public.update_timestamp;

CREATE TABLE public.update_timestamp (
	id bigserial NOT NULL,
	table_name varchar(64) NOT NULL,
	update_timestamp timestamp NOT NULL,
	CONSTRAINT update_timestamp_pk PRIMARY KEY (id),
	CONSTRAINT update_timestamp_un UNIQUE (table_name)
);

-- Permissions

ALTER TABLE public.update_timestamp OWNER TO pgroot;
GRANT ALL ON TABLE public.update_timestamp TO pgroot;
