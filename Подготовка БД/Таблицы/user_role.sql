-- public.user_role definition

-- Drop table

-- DROP TABLE public.user_role;

CREATE TABLE public.user_role (
	id bigserial NOT NULL,
	user_id int8 NOT NULL,
	role_id int8 NOT NULL,
	update_timestamp timestamp NOT NULL,
	CONSTRAINT user_role_pk PRIMARY KEY (id),
	CONSTRAINT user_role_un UNIQUE (user_id, role_id)
);

-- Table Triggers

create trigger user_role_update_timestamp_column_trigger before
insert
    or
update
    of user_id,
    role_id on
    user_role for each row execute procedure user_role_update_timestamp_column_function();

-- Permissions

ALTER TABLE public.user_role OWNER TO pgroot;
GRANT ALL ON TABLE public.user_role TO pgroot;


-- public.user_role foreign keys

ALTER TABLE public.user_role ADD CONSTRAINT user_role_fk_role FOREIGN KEY (role_id) REFERENCES my_role(id);
ALTER TABLE public.user_role ADD CONSTRAINT user_role_fk_user FOREIGN KEY (user_id) REFERENCES my_user(id);
