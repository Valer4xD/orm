-- public.my_role definition

-- Drop table

-- DROP TABLE public.my_role;

CREATE TABLE public.my_role (
	id bigserial NOT NULL,
	"name" varchar(64) NOT NULL,
	CONSTRAINT my_role_pk PRIMARY KEY (id),
	CONSTRAINT my_role_un UNIQUE (name)
);

-- Permissions

ALTER TABLE public.my_role OWNER TO pgroot;
GRANT ALL ON TABLE public.my_role TO pgroot;
