-- public.my_mail definition

-- Drop table

-- DROP TABLE public.my_mail;

CREATE TABLE public.my_mail (
	id bigserial NOT NULL,
	address varchar(64) NOT NULL,
	CONSTRAINT my_mail_pk PRIMARY KEY (id),
	CONSTRAINT my_mail_un UNIQUE (address)
);

-- Table Triggers

create trigger my_mail_update_timestamp_table_trigger before
insert
    or
update
    of address on
    my_mail for each statement execute procedure my_mail_update_timestamp_table_function();

-- Permissions

ALTER TABLE public.my_mail OWNER TO pgroot;
GRANT ALL ON TABLE public.my_mail TO pgroot;
