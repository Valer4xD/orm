-- public.my_site definition

-- Drop table

-- DROP TABLE public.my_site;

CREATE TABLE public.my_site (
	id bigserial NOT NULL,
	url varchar(64) NOT NULL,
	CONSTRAINT my_site_pk PRIMARY KEY (id),
	CONSTRAINT my_site_un UNIQUE (url)
);

-- Permissions

ALTER TABLE public.my_site OWNER TO pgroot;
GRANT ALL ON TABLE public.my_site TO pgroot;
