CREATE OR REPLACE FUNCTION public.my_user_update_timestamp_column_function()
	RETURNS TRIGGER
	LANGUAGE PLPGSQL
AS $function$
	BEGIN
		NEW.update_timestamp = NOW()::TIMESTAMPTZ(6) AT TIME ZONE 'UTC';
		RETURN NEW;
	END;
$function$;
