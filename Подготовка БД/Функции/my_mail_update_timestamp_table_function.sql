CREATE OR REPLACE FUNCTION public.my_mail_update_timestamp_table_function()
	RETURNS TRIGGER
	LANGUAGE PLPGSQL
AS $function$
	BEGIN
		UPDATE update_timestamp SET update_timestamp = NOW()::TIMESTAMPTZ(6) AT TIME ZONE 'UTC'
            WHERE table_name = 'my_mail';
		RETURN NEW;
	END;
$function$;
