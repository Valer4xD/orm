﻿using CacheDB;
using Npgsql;
using System;
using System.ServiceModel;

namespace Service
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "Server = localhost; Port = 5432; User Id = pgroot; Password = 1234; Database = nhibernate";
            NpgsqlConnection connection = new NpgsqlConnection(connectionString);
            connection.Open();

            new DAOEntity().Connection = connection;

            var serviceAddress = "127.0.0.1:10000";
            var serviceName = "MyService";
            var host = new ServiceHost(typeof(DAOEntity), new Uri($"net.tcp://{serviceAddress}/{serviceName}"));
            var serverBinding = new NetTcpBinding();
            host.AddServiceEndpoint(typeof(IDAOEntity), serverBinding, "");
            host.Open();

            Console.ReadKey();
        }
    }
}
